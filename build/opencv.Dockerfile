FROM nvcr.io/nvidia/deepstream-l4t:5.1-21.02-base

ENV PACKAGES="ffmpeg git libgstreamer-plugins-good1.0-0 libgstreamer-plugins-good1.0-dev gstreamer1.0-plugins-good make sqlite3 gcc x264 wget ca-certificates libopencv-dev pkg-config zip g++ zlib1g-dev unzip python python3-numpy tar libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev sudo libavcodec-dev libavformat-dev libswscale-dev libjpeg-dev libpng-dev libtiff-dev build-essential cmake unzip pkg-config libatlas-base-dev gfortran libv4l-dev libxvidcore-dev libx264-dev curl wget"

ENV OPENCV_VERSION=4.5.1
ENV TMP_DIR=/tmp/
ENV BUILD_SHARED_LIBS=ON
WORKDIR /

RUN DEBIAN_FRONTEND=noninteractive apt-get update && DEBIAN_FRONTEND=noninteractive apt-get upgrade -y &&  DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends --yes ${PACKAGES}

RUN mkdir /temp \ 
&& wget -nv https://github.com/opencv/opencv_contrib/archive/${OPENCV_VERSION}.zip -O /temp/opencvcontrib-${OPENCV_VERSION}.zip \
&& wget -nv https://github.com/opencv/opencv/archive/${OPENCV_VERSION}.zip -O /temp/opencv-${OPENCV_VERSION}.zip

RUN unzip /temp/opencv-${OPENCV_VERSION}.zip\
&& unzip /temp/opencvcontrib-${OPENCV_VERSION}.zip
RUN mkdir -p /opencv-${OPENCV_VERSION}/build && cd /opencv-${OPENCV_VERSION}/build && rm -rf * && cmake -j 8 -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D BUILD_SHARED_LIBS=${BUILD_SHARED_LIBS} -D OPENCV_EXTRA_MODULES_PATH=/opencv_contrib-${OPENCV_VERSION}/modules -D BUILD_DOCS=OFF -D BUILD_EXAMPLES=OFF -D BUILD_TESTS=OFF -D BUILD_PERF_TESTS=OFF -D BUILD_opencv_java=NO -D BUILD_opencv_python=NO -D BUILD_opencv_python2=NO -D BUILD_opencv_python3=NO -D WITH_JASPER=OFF -DOPENCV_GENERATE_PKGCONFIG=ON -D WITH_GSTREAMER=ON -D OPENCV_ENABLE_NONFREE=ON -D WITH_CUDA=ON -D WITH_CUDNN=ON -D OPENCV_DNN_CUDA=ON -D ENABLE_FAST_MATH=1 -D CUDA_FAST_MATH=1 -D CUDA_ARCH_BIN=7.0 -D CUDA_ARCH_PTX:STRING="5.0 6.1 7.0"  -D WITH_CUBLAS=1 -DCUDA_TOOLKIT_ROOT_DIR=/usr/local/cuda/ .. && make -j 8
RUN cd /opencv-${OPENCV_VERSION}/build && make install

