ARG BUILD_ID
FROM registry.gitlab.com/skyhuborg/tracker/opencv-build:${BUILD_ID} AS opencv-builder
FROM nvcr.io/nvidia/deepstream-l4t:5.1-21.02-base
COPY --from=opencv-builder /usr/local/lib/libopencv_stereo.so /usr/local/lib/libopencv_aruco.so /usr/local/lib/libopencv_cudaimgproc.so /usr/local/lib/libopencv_datasets.so /usr/local/lib/libopencv_plot.so /usr/local/lib/libopencv_face.so /usr/local/lib/libopencv_surface_matching.so /usr/local/lib/libopencv_stitching.so /usr/local/lib/libopencv_dnn_superres.so /usr/local/lib/libopencv_cudafeatures2d.so /usr/local/lib/libopencv_intensity_transform.so /usr/local/lib/libopencv_optflow.so /usr/local/lib/libopencv_videostab.so /usr/local/lib/libopencv_shape.so /usr/local/lib/libopencv_rapid.so /usr/local/lib/libopencv_cudawarping.so /usr/local/lib/libopencv_mcc.so /usr/local/lib/libopencv_cudaoptflow.so /usr/local/lib/libopencv_structured_light.so /usr/local/lib/libopencv_photo.so /usr/local/lib/libopencv_bioinspired.so /usr/local/lib/libopencv_video.so /usr/local/lib/libopencv_objdetect.so /usr/local/lib/libopencv_fuzzy.so /usr/local/lib/libopencv_hfs.so /usr/local/lib/libopencv_superres.so /usr/local/lib/libopencv_imgcodecs.so /usr/local/lib/libopencv_saliency.so /usr/local/lib/libopencv_videoio.so /usr/local/lib/libopencv_xfeatures2d.so /usr/local/lib/libopencv_imgproc.so /usr/local/lib/libopencv_dpm.so /usr/local/lib/libopencv_ximgproc.so /usr/local/lib/libopencv_xphoto.so /usr/local/lib/libopencv_line_descriptor.so /usr/local/lib/libopencv_highgui.so /usr/local/lib/libopencv_calib3d.so /usr/local/lib/libopencv_phase_unwrapping.so /usr/local/lib/libopencv_cudalegacy.so /usr/local/lib/libopencv_cudafilters.so /usr/local/lib/libopencv_xobjdetect.so /usr/local/lib/libopencv_text.so /usr/local/lib/libopencv_flann.so /usr/local/lib/libopencv_cudaobjdetect.so /usr/local/lib/libopencv_tracking.so /usr/local/lib/libopencv_ml.so /usr/local/lib/libopencv_cudev.so /usr/local/lib/libopencv_quality.so /usr/local/lib/libopencv_features2d.so /usr/local/lib/libopencv_ccalib.so /usr/local/lib/libopencv_cudaarithm.so /usr/local/lib/libopencv_cudabgsegm.so /usr/local/lib/libopencv_core.so /usr/local/lib/libopencv_gapi.so /usr/local/lib/libopencv_reg.so /usr/local/lib/libopencv_cudacodec.so /usr/local/lib/libopencv_bgsegm.so /usr/local/lib/libopencv_dnn.so /usr/local/lib/libopencv_img_hash.so /usr/local/lib/libopencv_rgbd.so /usr/local/lib/libopencv_cudastereo.so /usr/local/lib/libopencv_dnn_objdetect.so /usr/local/lib/
COPY --from=opencv-builder /usr/local/lib/pkgconfig/opencv4.pc /usr/local/lib/pkgconfig/
COPY --from=opencv-builder /usr/local/include/opencv4 /usr/local/include/opencv4/
COPY --from=opencv-builder /usr/local/bin/opencv_version /usr/local/bin/setup_vars_opencv4.sh /usr/local/bin/opencv_waldboost_detector /usr/local/bin/opencv_annotation /usr/local/bin/opencv_interactive-calibration /usr/local/bin/opencv_visualisation /usr/local/bin/

ENV PACKAGES="gcc g++ libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev"

RUN DEBIAN_FRONTEND=noninteractive apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends --yes ${PACKAGES}

ENV GOPATH=/go
ENV GO111MODULE=on
ENV PATH=${PATH}:/usr/local/go/bin
ENV GOVERSION=1.16.5

RUN wget https://golang.org/dl/go${GOVERSION}.linux-arm64.tar.gz && \
    tar -C /usr/local -xzf go${GOVERSION}.linux-arm64.tar.gz

# Object Tracking Tensorflow model
RUN mkdir /build && cd /build/ && wget http://download.tensorflow.org/models/object_detection/ssd_mobilenet_v1_coco_2017_11_17.tar.gz && tar xvfz ssd_mobilenet_v1_coco_2017_11_17.tar.gz && wget https://gist.githubusercontent.com/dkurt/45118a9c57c38677b65d6953ae62924a/raw/b0edd9e8c992c25fe1c804e77b06d20a89064871/ssd_mobilenet_v1_coco_2017_11_17.pbtxt


ARG BUILD_ID
COPY go.mod /build/go.mod
COPY go.sum /build/go.sum
RUN cd /build && go mod download

ADD . /build/
RUN cd /build && \
    BUILD_ID=${BUILD_ID} make tracker


