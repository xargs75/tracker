/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel, Corey Gaspard

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package controller

import (
	"context"
	"fmt"
	"log"
	"math"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"github.com/golang/protobuf/ptypes"
	guuid "github.com/google/uuid"
	"github.com/improbable-eng/grpc-web/go/grpcweb"
	pb "gitlab.com/skyhuborg/tracker/internal/proto"
	"gitlab.com/skyhuborg/tracker/pkg/config"
	"gitlab.com/skyhuborg/tracker/pkg/db"
	"golang.org/x/crypto/bcrypt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/grpclog"
	"google.golang.org/protobuf/proto"
)

type Server struct {
	pb.UnimplementedTrackerdServer
	pb.UnimplementedControllerServer
	Handle          *grpc.Server
	HandleTrackerd  *grpc.Server
	ListenPort      int
	EnableTls       bool
	TlsKey          string
	TlsCert         string
	ConfigFile      string
	DbConnectString string
	DbDriver        string
	StaticDataPath  string
	PipeFilePath    string
	StaticDataPort  int
	AuthTokens      []Auth
	SensorReport    pb.SensorReport

	config config.Config
	db     db.DB
}

type Auth struct {
	Token    string
	Username string
	Expires  time.Time
}

func (s *Server) OpenConfig() (err error) {
	err = s.config.Open(s.ConfigFile)

	if err != nil {
		grpclog.Infof("Error: failed opening configuration: %s", s.ConfigFile)
		return err
	}
	return nil
}

func (s *Server) ConnectDb() error {
	db := db.DB{}

	err := db.Open(s.DbDriver, s.DbConnectString)

	if err != nil {
		grpclog.Infof("Error: %s\n")
		return err
	}

	s.db = db
	return nil
}

func (s *Server) StartFileServer() error {
	grpclog.Infof("Service '%s' on :%d\n", s.StaticDataPath, s.StaticDataPort)
	fs := http.FileServer(http.Dir(s.StaticDataPath))
	http.Handle("/", fs)

	err := http.ListenAndServe(fmt.Sprintf(":%d", s.StaticDataPort), nil)
	if err != nil {
		grpclog.Fatal(err)
	}
	return nil
}

func (s *Server) Close() {
	s.config.Close()
	s.db.Close()
}

func (s *Server) Start() {
	var (
		err error
	)

	err = s.OpenConfig()

	if err != nil {
		grpclog.Infof("OpenConfig failed: %s\n", err)
		return
	}

	err = s.ConnectDb()

	if err != nil {
		grpclog.Infof("ConnectDb failed with: %s\n", err)
		return
	}

	go s.StartFileServer()

	s.Handle = grpc.NewServer()
	s.HandleTrackerd = grpc.NewServer()

	pb.RegisterControllerServer(s.Handle, s)
	//pb.RegisterTrackerdServer(s.HandleTrackerd, s)

	grpclog.SetLogger(log.New(os.Stdout, "tracker-controller: ", log.LstdFlags))

	wrappedServer := grpcweb.WrapServer(s.Handle,
		grpcweb.WithOriginFunc(func(origin string) bool {
			return true
		}))

	handler := func(resp http.ResponseWriter, req *http.Request) {
		wrappedServer.ServeHTTP(resp, req)
	}

	httpServer := http.Server{
		Addr:    fmt.Sprintf(":%d", s.ListenPort),
		Handler: http.HandlerFunc(handler),
	}

	port := 8089
	grpclog.Infof("Starting sensor relay server port %d", port)
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		grpclog.Fatalf("failed to listen: %v", err)
	}
	go s.HandleTrackerd.Serve(lis)

	grpclog.Infof("Starting server without TLS on port %d", s.ListenPort)
	if err := httpServer.ListenAndServe(); err != nil {
		grpclog.Fatalf("failed starting http server: %v", err)
	}

}

// func (s *Server) TrackerStatusStream(
// 	request *pb_hmi.DflyStatusRequest,
// 	stream pb_hmi.Dfly_DflyStatusStreamServer) error {

// 	done := make(chan bool)

// 	go func() {
// 		for {
// 			reply, valid := s.status_q.Pop(true)
// 			if !valid {
// 				return
// 			}

// 			err := stream.SendMsg(reply)
// 			if err != nil {
// 				done <- true
// 				return
// 			}
// 		}
// 	}()

// 	<-done
// 	return nil
// }

func (s *Server) SetConfig(ctx context.Context, in *pb.SetConfigReq) (*pb.SetConfigResp, error) {
	r := pb.SetConfigResp{}

	grpclog.Infof("SetConfig called\n")
	s.config.SetConfigFromPb(in.Config)
	s.config.Save()

	writeToFile(s.PipeFilePath, "RESTART-TRACKER\n")

	return &r, nil
}

func (s *Server) Ping(ctx context.Context, in *pb.PingReq) (*pb.PingResp, error) {
	resp := pb.PingResp{}
	resp.Status = "OK"
	return &resp, nil
}

func (s *Server) IssueCommand(ctx context.Context, in *pb.IssueCommandReq) (*pb.IssueCommandResp, error) {
	resp := pb.IssueCommandResp{}

	grpclog.Infof("Executing command on host: %s\n", in.Command)
	if isAuthenticated(in.Authtoken, s.AuthTokens) {
		//_, err := s.PipeFile.WriteString(fmt.Sprintf("%s\n", in.Command))
		grpclog.Infof("Writing file %s\n", s.PipeFilePath)
		err := writeToFile(s.PipeFilePath, fmt.Sprintf("%s\n", in.Command))
		if err != nil {
			resp.Status = "failed"
			resp.Message = err.Error()
		} else {
			resp.Status = "success"
			resp.Message = "Success"
		}
		return &resp, nil
	}
	resp.Status = "failed"
	resp.Message = fmt.Sprintf("Not Authorized Error: %s", in.Command)
	return &resp, nil
}

func (s *Server) Login(ctx context.Context, in *pb.LoginReq) (*pb.LoginResp, error) {
	resp := pb.LoginResp{}

	/// Logging in with an Auth Token
	if len(in.Authtoken) > 0 {
		if isAuthenticated(in.Authtoken, s.AuthTokens) {
			resp.Success = true
			resp.Authtoken = in.Authtoken
			return &resp, nil
		}

		resp.Success = false
		resp.Authtoken = ""
		resp.Authexpired = true
		return &resp, nil

	}

	conf := s.config.GetConfigPb()
	success := comparePasswords(conf.Password, in.Password)
	if in.Username == conf.Username && success {
		uuid, err := guuid.NewRandom()
		if err != nil {
			grpclog.Infoln("Error generatoring auth token")
		}
		resp.Success = true
		resp.Authtoken = uuid.String()
		expDate := time.Now()
		expDate = expDate.AddDate(0, 0, 7*2)
		authToken := Auth{Token: uuid.String(), Username: in.Username, Expires: expDate}
		s.AuthTokens = append(s.AuthTokens, authToken)
	} else {
		resp.Success = false
		resp.Message = "Invalid Username or Password"
	}
	return &resp, nil
}

func (s *Server) AddSensor(ctx context.Context, in *pb.SensorReport) (*pb.SensorReportResponse, error) {
	grpclog.Infof("Report received\n")
	s.SensorReport = *in
	return &pb.SensorReportResponse{}, nil
}

func (s *Server) AddEvent(ctx context.Context, in *pb.Event) (*pb.EventResponse, error) {
	return &pb.EventResponse{}, nil
}
func (s *Server) AddVideoEvent(ctx context.Context, in *pb.VideoEvent) (*pb.VideoEventResponse, error) {
	return &pb.VideoEventResponse{}, nil
}
func (s *Server) Register(ctx context.Context, in *pb.TrackerInfo) (*pb.RegisterResponse, error) {
	return &pb.RegisterResponse{}, nil
}

func (s *Server) GetSensorReport(ctx context.Context, in *pb.SensorReportReq) (*pb.SensorReport, error) {
	//grpclog.Infof("GetSensorReport Called")
	sensors, _ := s.db.GetSensors(1)
	report := &pb.SensorReport{}
	report.LonLat = &pb.LonLat{}
	if len(sensors) > 0 {
		proto.Unmarshal(sensors[0].Data, report)
	}
	return report, nil
}

func (s *Server) GetSensorReports(ctx context.Context, in *pb.GetSensorReportsReq) (*pb.GetSensorReportsResp, error) {
	grpclog.Infof("GetSensorReports Called")
	r := pb.GetSensorReportsResp{}
	return &r, nil
}

func (s *Server) GetContainerList(ctx context.Context, in *pb.GetContainerListReq) (*pb.GetContainerListResp, error) {
	r := pb.GetContainerListResp{}

	// r.Video = append(r.Video, &pb.VideoEvent{EventId: e.EventId, CreatedAt: ts, Uri: uri, Thumb: thumb, WebUri: web_uri})
	var containers = s.listContainers()
	for _, c := range containers {
		var cont = &pb.Container{}
		cont.Id = c.Id
		cont.Name = c.Name
		cont.Status = c.Status
		cont.Image = c.Image
		r.Containers = append(r.Containers, cont)
	}
	//r.Containers = s.ListContainers()
	return &r, nil
}

func (s *Server) GetContainerLog(ctx context.Context, in *pb.GetContainerLogReq) (*pb.GetContainerLogResp, error) {
	r := pb.GetContainerLogResp{}
	r.Log = s.getContainerLog(in.Containerid)
	//r.Containers = s.ListContainers()
	return &r, nil
}

// func (s *Server) GetStatusReport(ctx context.Context, in *pb.GetStatusReportReq) (*pb.GetStatusReportResp, error) {
// 	grpclog.Infof("GetStatusReportCalled")
// 	r := pb.GetStatusReportResp{}
// 	return &r, nil
// }

func (s *Server) GetIsConfigured(ctx context.Context, in *pb.GetIsConfiguredReq) (*pb.GetIsConfiguredResp, error) {
	r := pb.GetIsConfiguredResp{}

	grpclog.Infof("GetIsConfigured called\n")

	r.IsConfigured = s.config.GetIsConfigured()

	return &r, nil
}

func (s *Server) GetConfig(ctx context.Context, in *pb.GetConfigReq) (*pb.GetConfigResp, error) {
	r := pb.GetConfigResp{}
	var err error

	grpclog.Infof("GetConfig called\n")

	r.Config, err = s.config.GetConfigFromFile(s.ConfigFile)
	r.Config.Password = ""
	r.Config.PasswordAgain = ""

	if err != nil {
		return nil, err
	}

	return &r, nil
}

func (s *Server) GetEvents(ctx context.Context, in *pb.GetEventsReq) (*pb.GetEventsResp, error) {
	r := &pb.GetEventsResp{}
	var err error

	events, total, err := s.db.GetEvents(int(in.Limit))

	for _, e := range events {
		ts, _ := ptypes.TimestampProto(e.CreatedAt)

		es := &pb.EventSource{Name: e.Source, Sensor: e.Sensor}
		// TODO, add proper source
		r.Event = append(r.Event, &pb.Event{Uuid: e.Id, StartTime: ts, EndTime: ts, Duration: e.Duration /*Type: e.Type,*/, Source: es})
	}

	f_total := float64(total)
	f_limit := float64(in.Limit)
	n_pages := int32(math.Ceil(f_total / f_limit))

	r.Npages = n_pages
	r.Page = int32(in.Page)
	r.Total = int32(total)

	return r, err
}

func (s *Server) GetEvent(ctx context.Context, in *pb.GetEventReq) (*pb.GetEventResp, error) {
	r := &pb.GetEventResp{}
	var err error

	event, _ := s.db.GetEvent(in.Eventid)
	sensors, _ := s.db.GetSensorsForEvent(in.Eventid)
	videos, _ := s.db.GetVideosForEvent(in.Eventid)

	info := &pb.TrackerInfo{Uuid: s.config.GetUuid(), Name: s.config.NodeName}
	st, _ := ptypes.TimestampProto(event.StartedAt)
	et, _ := ptypes.TimestampProto(event.EndedAt)

	r.Event = &pb.Event{
		Tracker:   info,
		Uuid:      event.Id,
		Source:    &pb.EventSource{},
		StartTime: st,
		EndTime:   et,
		Duration:  event.Duration,
		//Type:      event.Type,
	}

	for _, e := range videos {
		//ts, _ := ptypes.TimestampProto(e.CreatedAt)
		base := filepath.Base(e.Uri)
		// uri := fmt.Sprintf("http://localhost:3000/video/%s", base)
		uri := base
		web_uri := filepath.Base(e.WebUri)
		base = filepath.Base(e.Thumbnail)
		// thumb := fmt.Sprintf("http://localhost:3000/thumbnail/%s", base)
		thumb := base

		r.Videos = append(r.Videos, &pb.VideoEvent{EventUuid: e.EventId, Uri: uri, Thumbnail: thumb, WebUri: web_uri})
	}

	for _, sensor := range sensors {
		report := &pb.SensorReport{}
		proto.Unmarshal(sensor.Data, report)
		r.Reports = append(r.Reports, report)
	}

	return r, err
}

func (s *Server) GetVideoEvents(ctx context.Context, in *pb.GetVideoEventsReq) (*pb.GetVideoEventsResp, error) {
	r := &pb.GetVideoEventsResp{}
	var err error

	grpclog.Infoln("GetVideoEvents called")

	events, total, err := s.db.GetVideoEvents(int(in.Limit), int(in.Page))

	for _, e := range events {
		ts, _ := ptypes.TimestampProto(e.CreatedAt)
		base := filepath.Base(e.Uri)
		// uri := fmt.Sprintf("http://localhost:3000/video/%s", base)
		uri := base
		web_uri := filepath.Base(e.WebUri)
		base = filepath.Base(e.Thumbnail)
		// thumb := fmt.Sprintf("http://localhost:3000/thumbnail/%s", base)
		thumb := base

		r.Video = append(r.Video, &pb.VideoEvent{EventUuid: e.EventId, Uri: uri, Thumbnail: thumb, WebUri: web_uri, StartTime: ts})
	}

	f_total := float64(total)
	f_limit := float64(in.Limit)
	n_pages := int32(math.Ceil(f_total / f_limit))

	r.Npages = n_pages
	r.Page = in.Page
	r.Total = int32(total)

	return r, err
}

func writeToFile(filename string, data string) error {

	_, err := os.Stat(filename)
	var file *os.File
	var fileerr error
	if !os.IsNotExist(err) {
		file, fileerr = os.OpenFile(filename, os.O_RDWR, os.ModeNamedPipe)
	} else {
		file, fileerr = os.Create(filename)
	}

	if fileerr != nil {
		return fileerr
	}
	defer file.Close()

	_, err = file.WriteString(data)
	if err != nil {
		return err
	}
	return file.Sync()
}

func isAuthenticated(authToken string, authTokens []Auth) bool {
	curDate := time.Now()
	for _, a := range authTokens {
		/// Token is a match and it is not expired
		if a.Token == authToken && curDate.Before(a.Expires) {
			return true
		}
	}
	return false
}

func comparePasswords(hashedPwd string, plainPwd string) bool {
	// Since we'll be getting the hashed password from the DB it
	// will be a string so we'll need to convert it to a byte slice
	var bytePass = []byte(plainPwd)
	byteHash := []byte(hashedPwd)
	err := bcrypt.CompareHashAndPassword(byteHash, bytePass)
	if err != nil {
		grpclog.Infoln(err)
		return false
	}

	return true
}
