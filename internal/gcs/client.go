package gcs

import (
	"cloud.google.com/go/storage"
	"context"
	"crypto/tls"
	"fmt"
	"google.golang.org/api/iterator"
	"google.golang.org/api/option"
	"io"
	"log"
	"net/http"
	"os"
	"path"
)

type Client struct {
	config *ClientConfig
	handle *storage.Client
	ctx    context.Context
	bucket *storage.BucketHandle
}

type ClientConfig struct {
	DownloadPath  string
	Bucket        string
	EnableDevMode bool
}

func NewClient(config *ClientConfig) *Client {
	client := Client{
		config: config,
	}
	return &client
}

func (c *Client) Open() (err error) {

	c.ctx = context.Background()

	if c.config.EnableDevMode {
		log.Println("Sync: Development mode enabled")
		transCfg := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}

		httpClient := &http.Client{Transport: transCfg}
		c.handle, err = storage.NewClient(context.TODO(), option.WithEndpoint("https://0.0.0.0:4443/storage/v1/"), option.WithHTTPClient(httpClient))
	} else {
		c.handle, err = storage.NewClient(c.ctx)
	}

	if err != nil {
		goto exit
	}

	c.bucket = c.handle.Bucket(c.config.Bucket)
exit:
	return err
}

func (c *Client) List() ([]string, error) {
	query := &storage.Query{Prefix: ""}

	var names []string
	it := c.bucket.Objects(c.ctx, query)

	for {
		attrs, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		names = append(names, attrs.Name)
	}

	return names, nil
}

func (c *Client) Get(filename string) error {
	var (
		err     error = nil
		fileUri string
		f       *os.File
		r       io.Reader
	)

	obj := c.bucket.Object(filename)

	r, err = obj.NewReader(c.ctx)

	if err != nil {
		goto exit
	}

	fileUri = fmt.Sprintf("%s/%s", c.config.DownloadPath, path.Base(filename))

	f, err = os.Create(fileUri)

	if err != nil {
		goto exit
	}
	defer f.Close()

	_, err = io.Copy(f, r)

	if err != nil {
		goto exit
	}
exit:
	return err
}

func (c *Client) Put(destFile string, sourceFile string) error {
	var (
		err    error = nil
		f      *os.File
		writer *storage.Writer
	)

	f, err = os.Open(sourceFile)

	if err != nil {
		goto exit
	}
	defer f.Close()

	writer = c.bucket.Object(destFile).NewWriter(c.ctx)

	if _, err = io.Copy(writer, f); err != nil {
		goto exit
	}
	if err := writer.Close(); err != nil {
		goto exit
	}

exit:
	return err
}

func (c *Client) Close() {
	c.handle.Close()

}

/*
func main() {
	var (
		err error
	)

	config := ClientConfig{
		ServiceFile: "sky-hub-01-e3a75f9eb4fb.json",
		DownloadPath: "download",
	}

	client := NewClient(&config)

	err = client.Open(&config)

	if err != nil {
		log.Println(err)
	}

	items, err := client.List()

	if err != nil {
		log.Println(err)
	}

	for _, item := range items {
		log.Println(item)
	}

	client.Get("test/93c01ddc-b649-482f-940c-2fab3a50dde5.mkv")
}*/
