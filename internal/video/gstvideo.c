/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
#include <string.h>
#include <gst/gst.h>
#include <gst/app/gstappsink.h>
#include <gst/rtsp/gstrtsptransport.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <gst/video/video-converter.h>

#include "gstvideo.h"


enum {
	VIDEO_SUCCESS = 0,
	VIDEO_ERROR_UNAUTHORIZED = 1,
};

enum {
	PLATFORM_UNKNOWN = 0,
	PLATFORM_MACOSX = 1,
	PLATFORM_LINUX = 2,
	PLATFORM_JETSON_NANO = 3,
	PLATFORM_JETSON_XAVIER_NX = 4,
	PLATFORM_JETSON_XAVIER_AGX = 5,
	PLATFORM_JETSON_TX2 = 6,
	PLATFORM_JETSON_TX1 = 7,
	PLATFORM_WINDOWS = 8,
};

static char *platform_name[] = {
	"unknown",
	"macosx",
	"linux",
	"jetson_nano",
	"jetson_xavier_nx",
	"jetson_xavier_agx",
	"jetson_tx2",
	"jetson_tx1",
	"windows",
};

static void 
sh_video_recording_finalize(sh_video_t *shv) 
{	
	gst_bin_remove(GST_BIN(shv->pipeline), shv->file_queue);
	gst_bin_remove(GST_BIN(shv->pipeline), shv->file_parse);
	gst_bin_remove(GST_BIN(shv->pipeline), shv->mux);
	gst_bin_remove(GST_BIN(shv->pipeline), shv->filesink);

	gst_element_set_state(shv->file_queue, GST_STATE_NULL);
	gst_element_set_state(shv->file_parse, GST_STATE_NULL);
	gst_element_set_state(shv->mux, GST_STATE_NULL);
	gst_element_set_state(shv->filesink, GST_STATE_NULL);

	gst_object_unref(shv->file_queue);
	gst_object_unref(shv->file_parse);
	gst_object_unref(shv->mux);
	gst_object_unref(shv->filesink);

	gst_element_release_request_pad (shv->tee, shv->teepad);
	gst_object_unref(shv->teepad);

	shv->is_recording = 0;
}

static gboolean
message_cb (
	GstBus * bus, 
	GstMessage * message, 
	gpointer user_data)
{
	sh_video_t *shv = (sh_video_t *) user_data;

	switch (GST_MESSAGE_TYPE (message)) {
    case GST_MESSAGE_ERROR:{
		GError *err = NULL;
		gchar *name, *debug = NULL;

		name = gst_object_get_path_string (message->src);
		gst_message_parse_error (message, &err, &debug);

		g_printerr ("ERROR:  element=%s message=%s\n", name, err->message);

		if (debug != NULL) {
			g_printerr ("DEBUG:\n%s\n", debug);
		}

		if (strcmp(err->message, "Unauthorized") == 0) {
			g_printerr ("Failed connecting to camera\n");
			exit(-1);
		}

		g_error_free (err);
		g_free (debug);
		g_free (name);

		g_main_loop_quit (shv->loop);
		break;
	}
    case GST_MESSAGE_WARNING:{
		GError *err = NULL;
		gchar *name, *debug = NULL;

		name = gst_object_get_path_string (message->src);
		gst_message_parse_warning (message, &err, &debug);

		g_printerr ("ERROR:  element=%s message=%s\n", name, err->message);

		if (debug != NULL) {
			g_printerr ("DEBUG:\n%s\n", debug);
		}

		g_error_free (err);
		g_free (debug);
		g_free (name);
		break;
    }
    case GST_MESSAGE_EOS: {
		break;
	}
	case GST_MESSAGE_ELEMENT: {
    	const GstStructure *s = gst_message_get_structure (message);

    	if (gst_structure_has_name (s, "GstBinForwarded")) {
			GstMessage *forward_msg = NULL;

			gst_structure_get (s, "message", GST_TYPE_MESSAGE, &forward_msg, NULL);

			if (GST_MESSAGE_TYPE (forward_msg) == GST_MESSAGE_EOS) {
				sh_video_recording_finalize(shv);
			}
			gst_message_unref (forward_msg);
    	}
    	break;
	}
    default:
		break;
  }

  return TRUE;
}

static GstPadProbeReturn
unlink_from_tee_cb(GstPad *pad, 
	GstPadProbeInfo *info, 
	gpointer user_data) 
{
	sh_video_t *shv = (sh_video_t *) user_data;
	GstPad *sinkpad = NULL;
	sinkpad = gst_element_get_static_pad (shv->file_queue, "sink");
	gst_pad_unlink (shv->teepad, sinkpad);
	gst_object_unref (sinkpad);

	gst_element_send_event(shv->file_parse, gst_event_new_eos()); 

	return GST_PAD_PROBE_REMOVE;
}

static void 
rtspsrc_pad_added(
	GstElement *element, 
	GstPad *pad, 
	void *data)
{
	sh_video_t *shv = (sh_video_t *) data;
	GstPad *sinkpad = NULL;

	sinkpad = gst_element_get_static_pad(shv->depay, "sink");
	gst_pad_link (pad, sinkpad);
	gst_object_unref (sinkpad);
}

static int detect_platform() {
	char *jetson_module = NULL;
	int platform = PLATFORM_UNKNOWN;

	#ifdef _WIN32
		jetson_module = PLATFORM_WINDOWS;
	#elif __APPLE__
		jetson_module = PLATFORM_MACOSX;
	#elif __linux__
		jetson_module = getenv("JETSON_MODULE");

		if (jetson_module) {
			if (strcmp(jetson_module, "P2888-0001") == 0 || // Xavier AGX 8GB
					strcmp(jetson_module, "P2888-0006") == 0 || // Xavier AGX 16GB
					strcmp(jetson_module, "P2888-000x") == 0 ) { // Xavier AGX 32GB
				platform = PLATFORM_JETSON_XAVIER_AGX;
			} else if (strcmp(jetson_module, "P3448-0000") == 0 || // Nano Dev Kit version
						strcmp(jetson_module, "P3448-0002") == 0) { // Nano module
				platform = PLATFORM_JETSON_NANO;
			} else if (strcmp(jetson_module, "P3668") == 0 || // Xavier NX dev kit version
						strcmp(jetson_module, "3668-0000") == 0 ||
						strcmp(jetson_module, "3668-0001") == 0) { // Xavier NX module
				platform = PLATFORM_JETSON_XAVIER_NX;
			} else if (strcmp(jetson_module, "P3310-1000") == 0 || // TX2
						strcmp(jetson_module, "P3489-0000") == 0 || // TX2i
						strcmp(jetson_module, "3489-0888") == 0) { // TX2 4GB
				platform = PLATFORM_JETSON_TX2;
			} else if (strcmp(jetson_module, "P2180-1000") == 0) { // TX1
				platform = PLATFORM_JETSON_TX1;
			} else {
				// not sure what module this is, default to Jetson Nano
				platform = PLATFORM_JETSON_NANO;
			}
		} else {
			platform = PLATFORM_LINUX;
		}
	#else
		platform = PLATFORM_UNKNOWN;
	#endif
	
	return platform;
}

sh_video_t * sh_video_new(
	char *storage_path)
{
	sh_video_t *shv = NULL;
	int rc = 0;

	gst_init(NULL, NULL);

	shv = malloc(sizeof(sh_video_t));

	if (shv == NULL) {
		goto exit;
	}

	memset(shv, 0, sizeof(sh_video_t));

	shv->n_cores = sysconf(_SC_NPROCESSORS_ONLN);
	shv->platform = detect_platform();

	printf("Configuring pipeline for platform '%s'\n", platform_name[shv->platform]);

	shv->storage_path = strdup(storage_path);
	
	if (shv->storage_path == NULL) {
		rc = -1;
		goto exit;
	}

exit:
	if (rc != 0) {
		sh_video_free(shv);
	}
	return shv;
}

int sh_video_open(sh_video_t *shv, sh_video_src_t *src) 
{
	int rc = 0;
	//GstCaps *bgrcaps = NULL;
	GstCaps *bgrxcaps = NULL;

	if (shv == NULL || src == NULL) {
		rc = -1;
		goto exit;
	}

	shv->video_src = src;

	shv->pipeline = (GstElement *) gst_pipeline_new("pipeline");

	shv->src = gst_element_factory_make("rtspsrc", "src");
	shv->depay = gst_element_factory_make("rtph265depay", "depay");

	shv->tee = gst_element_factory_make("tee", "tee");

	shv->decode_queue = gst_element_factory_make("queue", "decode_queue");

    /*
	resizecaps = gst_caps_new_simple("video/x-raw",
		"width", G_TYPE_INT, 500,
		NULL);
    */

	// this is the size the ssd model needs
	//bgrcaps = gst_caps_new_simple("video/x-raw",
		//"width", G_TYPE_INT, 300,
		//"height", G_TYPE_INT, 300,
	//	"format", G_TYPE_STRING, "BGR",
	//	NULL);

	bgrxcaps = gst_caps_new_simple("video/x-raw",
	//"width", G_TYPE_INT, 300,
		//"height", G_TYPE_INT, 300,
        "width", G_TYPE_INT, 500,
		"format", G_TYPE_STRING, "GRAY8",
		NULL);

	if (shv->pipeline == NULL ||
			shv->src == NULL ||
			shv->depay == NULL ||
			shv->tee == NULL ||
			shv->decode_queue ==  NULL) {
		rc = -1;
		goto exit;
	}

	gst_bin_add_many(GST_BIN(shv->pipeline), 
		shv->src,
		shv->depay,
		shv->tee, 
		shv->decode_queue,
		NULL);

	if (!gst_element_link_many(shv->depay, shv->tee, NULL)) {
		rc = -1;
		goto exit;
	}

	// works on Linux and Mac OS X
	switch (shv->platform) {
		case PLATFORM_LINUX:
		case PLATFORM_MACOSX:
			shv->parse = gst_element_factory_make("h265parse", "parse");
			shv->decode = gst_element_factory_make("avdec_h265", "decode");
			shv->videoconvert = gst_element_factory_make("videoconvert", "convert");
			shv->bgr_filter = gst_element_factory_make("capsfilter", "bgr_filter");
			shv->videoscale = gst_element_factory_make("videoscale", "scale");
			shv->resize_filter = gst_element_factory_make("capsfilter", "resize_filter");
			shv->appsink = gst_element_factory_make("appsink", "appsink");
			//shv->appsink = gst_element_factory_make("autovideosink", "appsink");

			if (shv->parse == NULL ||
				shv->decode == NULL ||
				shv->videoconvert == NULL ||
				shv->appsink == NULL) {
				rc = -1;
				goto exit;
			}

			gst_bin_add_many(GST_BIN(shv->pipeline), 
				shv->parse, 
				shv->decode, 
				shv->videoscale,
				shv->resize_filter,
				shv->videoconvert, 
				shv->bgr_filter,
				shv->appsink, 
				NULL);


			if (!gst_element_link_many(shv->tee,
					shv->decode_queue,
					shv->parse,
					shv->decode,
					shv->videoscale,
					shv->resize_filter,
					shv->videoconvert, 
					shv->bgr_filter,
					shv->appsink, 
					NULL)) {
				rc = -1;
				goto exit;
			}

			g_object_set(shv->decode, "skip-frame", 1, NULL);

			break;
		case PLATFORM_JETSON_NANO:
		case PLATFORM_JETSON_XAVIER_NX:
		case PLATFORM_JETSON_XAVIER_AGX:
		case PLATFORM_JETSON_TX1:
		case PLATFORM_JETSON_TX2:
		default:
			shv->decode = gst_element_factory_make("nvv4l2decoder", "decode");
			shv->nvvidconv = gst_element_factory_make("nvvidconv", "nvvidconv");
			//shv->resize_filter = gst_element_factory_make("capsfilter", "resize_filter");
			shv->bgrx_filter = gst_element_factory_make("capsfilter", "bgrx_filter");
			//shv->videoconvert = gst_element_factory_make("videoconvert", "convert");
			//shv->bgr_filter = gst_element_factory_make("capsfilter", "bgr_filter");
			shv->appsink = gst_element_factory_make("appsink", "appsink");

			if (shv->decode == NULL ||
				shv->nvvidconv == NULL ||
				shv->bgrx_filter == NULL ||
				//shv->resize_filter == NULL ||
				//shv->bgr_filter == NULL ||
				//shv->videoconvert == NULL ||
				shv->appsink == NULL) {
				rc = -1;
				printf("Failed creating elements\n");
				goto exit;
			}

			gst_bin_add_many(GST_BIN(shv->pipeline), 
			shv->decode,
				shv->nvvidconv,
				//shv->resize_filter,
				shv->bgrx_filter,
				//shv->videoconvert, 
				//shv->bgr_filter,
				shv->appsink, 
				NULL);


			if (!gst_element_link_many(shv->tee,
					shv->decode_queue,
					shv->decode,
					shv->nvvidconv,
					shv->bgrx_filter,
					//shv->resize_filter,
					//shv->videoconvert, 
					//shv->bgr_filter,
					shv->appsink, 
					NULL)) {
				rc = -1;
				printf("Failed linking elements\n");
				goto exit;
			}

			// only for nvidia
			g_object_set(shv->decode, 
                "enable-max-performance", 1, 
                NULL);

			// bgrx filter
			g_object_set (G_OBJECT(shv->bgrx_filter),
				"caps", bgrxcaps, 
				NULL);
			gst_caps_unref (bgrxcaps);
			break;
	}

	// video resize filter
	//g_object_set (G_OBJECT(shv->resize_filter),
	//	"caps", resizecaps, 
	//	NULL);
	//gst_caps_unref (resizecaps);

	// bgr filter
	//g_object_set (G_OBJECT(shv->bgr_filter),
	//	"caps", bgrcaps, 
	//	NULL);
	//gst_caps_unref (bgrcaps);


	//g_object_set(G_OBJECT(shv->videoconvert),
	//	"matrix-mode", GST_VIDEO_MATRIX_MODE_NONE,
	//	"dither", GST_VIDEO_DITHER_NONE,
	//	"n-threads", 3,
	//	NULL);

	// rtspsrc settings
	g_signal_connect (shv->src, "pad-added", G_CALLBACK (rtspsrc_pad_added), shv);
	g_object_set(shv->src, "location", shv->video_src->uri, NULL);
	g_object_set(shv->src, "user-id", shv->video_src->username, NULL);
	g_object_set(shv->src, "user-pw", shv->video_src->password, NULL);
	g_object_set(shv->src, "protocols", GST_RTSP_LOWER_TRANS_TCP, NULL);
	g_object_set(GST_BIN(shv->pipeline), "message-forward", TRUE, NULL);

exit:
	return rc;
}

/* start the pipeline */
int sh_video_start(sh_video_t *shv) 
{
	int rc = 0;

	if (shv == NULL) {
		rc = -1;
		goto exit;
	}

	shv->loop = g_main_loop_new(NULL, FALSE);

	shv->bus = gst_pipeline_get_bus(GST_PIPELINE (shv->pipeline));
	gst_bus_add_signal_watch(shv->bus);
	g_signal_connect(G_OBJECT(shv->bus), "message", G_CALLBACK(message_cb), shv);
	gst_object_unref(GST_OBJECT(shv->bus));

	gst_element_set_state(shv->pipeline, GST_STATE_PLAYING);

	g_main_loop_run(shv->loop);

exit:
	return rc;
}

/* stop the pipeline */
int sh_video_stop(sh_video_t *shv) 
{
	int rc = 0;

	if (shv == NULL) {
		rc = -1;
		goto exit;
	}

exit:
	return rc;
}

/* start recording */
int sh_video_start_recording(sh_video_t *shv, char *event_uuid) 
{
	int rc = 0;
	GstPad *sinkpad;
	GstPadTemplate *templ;
	char fileUri[256] = {0};
	char thumbnailUri[256] = {0};

	if (shv == NULL || event_uuid == NULL) {
		rc = -1;
		goto exit;
	}

	shv->recording = malloc(sizeof(sh_video_recording_t));

	if (shv->recording == NULL) {
		rc = -1;
		goto exit;
	}
	memset(shv->recording, 0, sizeof(sh_video_recording_t));

	templ = gst_element_class_get_pad_template(GST_ELEMENT_GET_CLASS(shv->tee), "src_%u");
	shv->teepad = gst_element_request_pad(shv->tee, templ, NULL, NULL);
	
	shv->file_queue = gst_element_factory_make("queue", "file_queue");
	shv->file_parse = gst_element_factory_make("h265parse", "file_parse");
	shv->mux = gst_element_factory_make("matroskamux", "mux");
	shv->filesink = gst_element_factory_make("filesink", "filesink");

	sprintf(fileUri, "%s/video/%s.mkv", shv->storage_path, event_uuid);
	sprintf(thumbnailUri, "%s/thumbnail/%s.webp", shv->storage_path, event_uuid);

	shv->recording->event_id = strdup(event_uuid);
	shv->recording->video_uri = strdup(fileUri);
	shv->recording->thumbnail_uri = strdup(thumbnailUri);

	g_object_set(shv->filesink, "location", fileUri, NULL);

	gst_bin_add_many(GST_BIN(shv->pipeline),
		gst_object_ref(shv->file_queue),
		gst_object_ref(shv->file_parse),
		gst_object_ref(shv->mux),
		gst_object_ref(shv->filesink),
		NULL);


	sinkpad = gst_element_get_static_pad(shv->file_queue, "sink");
	gst_pad_link(shv->teepad, sinkpad);
	gst_object_unref(sinkpad);

	gst_element_link_many(
		shv->file_queue,
		shv->file_parse,
		shv->mux, 
		shv->filesink, 
		NULL);

	gst_element_sync_state_with_parent(shv->file_queue);
	gst_element_sync_state_with_parent(shv->file_parse);
	gst_element_sync_state_with_parent(shv->mux);
	gst_element_sync_state_with_parent(shv->filesink);

	gst_element_set_state(shv->file_queue, GST_STATE_PLAYING);
	gst_element_set_state(shv->file_parse, GST_STATE_PLAYING);
	gst_element_set_state(shv->mux, GST_STATE_PLAYING);
	gst_element_set_state(shv->filesink, GST_STATE_PLAYING);
	shv->is_recording = 1;

exit:
	return rc;
}

/* stop recording */
sh_video_recording_t *sh_video_stop_recording(sh_video_t *shv) 
{
	sh_video_recording_t *recording = NULL;

	if (shv == NULL || shv->recording == NULL) {
		goto exit;
	}

	GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN(shv->pipeline), GST_DEBUG_GRAPH_SHOW_ALL, "pipeline");

	gst_pad_add_probe(shv->teepad, GST_PAD_PROBE_TYPE_IDLE, unlink_from_tee_cb, shv, NULL);

	while (shv->is_recording == 1) {
		usleep(200);
	}
	recording = shv->recording;
	shv->recording = NULL;	
exit:
	return recording;
}

void sh_video_recording_free(sh_video_recording_t *recording)
{
	if (recording) {
		if (recording->event_id) {
			free(recording->event_id);
		}

		if (recording->video_uri) {
			free(recording->video_uri);
		}

		if (recording->thumbnail_uri) {
			free(recording->thumbnail_uri);
		}
		free(recording);
		recording = NULL;
	}
}

void sh_video_is_sink_ready(sh_video_t *shv) 
{
	GstState state = {0};

	if (shv == NULL) {
		goto exit;
	}

	// make sure we are in PLAYING state
	do {
		gst_element_get_state(shv->appsink,
			&state,
			NULL,
			GST_CLOCK_TIME_NONE);
	} while (state != GST_STATE_PLAYING);
exit:
	return;
}

int sh_video_get_is_recording(sh_video_t *shv) {
	int is_recording = 0;

	if (shv == NULL) {
		goto exit;
	}
	is_recording = shv->is_recording;
exit:
	return is_recording;
}

int sh_video_get_sink_frame(sh_video_t *shv, sh_video_frame_t *frame)
{
	int rc = 0;
    GstCaps *caps = NULL;
    GstStructure *structure = NULL;

	if (shv == NULL || frame == NULL) {
		rc =-1;
		goto exit;
	}

	// grab the video frame
	frame->sample = gst_app_sink_pull_sample((GstAppSink*)shv->appsink);
	
	if (frame->sample == NULL) {
		rc = -1;
		goto exit;
	}

	frame->buffer = gst_sample_get_buffer(frame->sample);

	// find out the structure of the frame
	if (frame->width == 0 || frame->height == 0) {
		caps = gst_sample_get_caps(frame->sample);
		structure = gst_caps_get_structure(caps, 0);
		frame->width = g_value_get_int(gst_structure_get_value(structure, "width"));
		frame->height = g_value_get_int(gst_structure_get_value(structure, "height"));
	}

    gst_buffer_map(frame->buffer, &frame->map, GST_MAP_READ);

	frame->dataSize = gst_buffer_get_size(frame->buffer);
	frame->data = frame->map.data;

exit:
	if (rc != 0) {
		sh_video_release_frame(frame);
	}
	
	return rc;
}

void sh_video_release_frame(sh_video_frame_t *frame) 
{
	if (frame) {
		// unref the previous buffer and frame
		if (frame->buffer) {
			gst_buffer_unmap(frame->buffer, &frame->map);
			frame->buffer = NULL;
			memset(&frame->map, 0, sizeof(sh_video_frame_t));
		}

		if (frame->sample) {
			gst_sample_unref(frame->sample);
		}
	}
	return;
}

void sh_video_free(sh_video_t *shv)
{
	if (shv) {
		if (shv->storage_path) {
			free(shv->storage_path);
		}

		memset(shv, 0, sizeof(sh_video_t));
		free(shv);
	}
	
	shv = NULL;

	return;
}

int sh_video_transcode(sh_video_t *shv, char *input, char *output)
{
	GstElement * pipeline = NULL;
	int rc = 0;
	const gchar * desc;
	GError *error = NULL;
	char launch[512] = {0};
	GstBus *bus = NULL;
	GstMessage *msg;
	int finished = FALSE;

	switch (shv->platform) {
		case PLATFORM_JETSON_NANO:
		case PLATFORM_JETSON_TX1:
			desc = "filesrc location=%s ! matroskademux ! h265parse ! nvv4l2decoder ! nvvidconv ! video/x-raw(memory:NVMM),width=(int)1280,height=(int)720,format=(string)NV12 ! nvv4l2vp8enc preset-level=0 bitrate=8000000 ! webmmux ! filesink location=%s";
			break;
		case PLATFORM_JETSON_XAVIER_NX:
		case PLATFORM_JETSON_XAVIER_AGX:
		case PLATFORM_JETSON_TX2:
			desc = "filesrc location=%s ! matroskademux ! h265parse ! nvv4l2decoder ! nvvidconv ! video/x-raw(memory:NVMM),width=(int)1280,height=(int)720,format=(string)NV12 ! nvv4l2vp9enc preset-level=0 bitrate=8000000 ! webmmux ! filesink location=%s";
			break;
		case PLATFORM_LINUX:
		case PLATFORM_MACOSX:
		case PLATFORM_WINDOWS:
		default:
			desc = "filesrc location=%s ! matroskademux ! h265parse ! avdec_h265 ! videoscale ! video/x-raw,width=(int)1280,height=(int)720 ! vp8enc ! webmmux ! filesink location=%s";
			break;
	}

	sprintf(launch, desc, input, output);

	pipeline = gst_parse_launch_full (launch, NULL, GST_PARSE_FLAG_FATAL_ERRORS, &error);

	if (pipeline == NULL || error) {
		printf("Parsing pipeline failed %s", error->message ? error->message : "none");
		goto exit;
	}

	bus = gst_pipeline_get_bus(GST_PIPELINE (pipeline));

	/* Playing the pipeline */
	gst_element_set_state (pipeline, GST_STATE_PLAYING);

	bus = gst_element_get_bus (pipeline);

	do {
		msg = gst_bus_timed_pop_filtered (bus, GST_CLOCK_TIME_NONE,
				GST_MESSAGE_STATE_CHANGED | GST_MESSAGE_ERROR | GST_MESSAGE_EOS);

		/* Parse message */
		if (msg != NULL) {
			GError *err;
			gchar *debug_info;

			switch (GST_MESSAGE_TYPE (msg)) {
				case GST_MESSAGE_ERROR:
					gst_message_parse_error (msg, &err, &debug_info);
					g_printerr ("Error %s: %s\n", GST_OBJECT_NAME (msg->src), err->message);
					g_printerr ("Debug: %s\n", debug_info ? debug_info : "none");
					g_clear_error (&err);
					g_free (debug_info);
					finished = TRUE;
					break;
				case GST_MESSAGE_EOS:
					finished = TRUE;
					break;
				default:
					break;
			}
			gst_message_unref (msg);
		}
	} while (!finished);

	/* NULL the pipeline */
	gst_element_set_state (pipeline, GST_STATE_NULL);
	gst_object_unref (pipeline);

	printf("Transcoded '%s' to '%s'\n", input, output);

exit:
	return rc;
}

int main2(int argc, char *argv[])
{
	int rc = 0;
	sh_video_t *shv = NULL;
	sh_video_src_t src = {0};

	shv = sh_video_new("storage");

	if (shv == NULL) {
		rc = -1;
		goto exit;
	}
	   rc = sh_video_start(shv);

	   if (rc != 0) {
	   rc = -1;
	   goto exit;
	   }

	   rc = sh_video_open(shv, &src);

	   if (rc != 0) {
	   rc = -1;
	   goto exit;
	   }

	   rc = sh_video_start_recording(shv, "test");

	   if (rc != 0) {
	   rc = -1;
	   goto exit;
	   }
	printf("rc = %d\n", sh_video_transcode(shv, argv[1], argv[2]));
exit:
	return rc;
}


