/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/


#ifndef _SH_VIDEO_H
#define _SH_VIDEO_H

#include <string.h>
#include <gst/gst.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

typedef struct _sh_video_src_t {
	int Type;
	char *uri;
	char *username;
	char *password;
	int height;
	int width;
	float fps;
	char *codec;
} sh_video_src_t;

typedef struct _sh_video_recording_t {
	char *event_id;
	char *video_uri;
	char *thumbnail_uri;
} sh_video_recording_t;

typedef struct _sh_video_t {
	GMainLoop *loop;
	GstBus 	*bus;
	int platform;
	int n_cores;

	sh_video_src_t *video_src;
	sh_video_recording_t *recording;
	
	int is_recording;
	char *storage_path;

	GstElement *pipeline;

	// core pipeline
	GstElement *src;
	GstElement *depay;
	GstElement *frame_queue;
	GstElement *tee;
	GstPad *teepad;

	// opencv pipeline
	GstElement *decode_queue;
	GstElement *parse;
	GstElement *decode;
	GstElement *videoconvert;
	GstElement *resize_filter;
	GstElement *bgr_filter;
	GstElement *videoscale;
	GstElement *nvvidconv;
	GstElement *bgrx_filter;
	GstElement *appsink;

	// recorder pipeline
	GstElement *file_queue;
	GstElement *file_parse;
	GstElement *mux;
	GstElement *filesink;
} sh_video_t;

typedef struct _sh_video_frame_t {
    GstSample *sample;
    GstBuffer *buffer;
	uint8_t *data;
	int dataSize;
    GstMapInfo map;
    int height;
    int width;
} sh_video_frame_t;

sh_video_t * sh_video_new(char *storage_path);
int sh_video_open(sh_video_t *shv, sh_video_src_t *src);
int sh_video_start(sh_video_t *shv);
int sh_video_stop(sh_video_t *shv);
int sh_video_start_recording(sh_video_t *shv, char *event_uuid);
sh_video_recording_t *sh_video_stop_recording(sh_video_t *shv);
void sh_video_is_sink_ready(sh_video_t *shv);
int sh_video_get_sink_frame(sh_video_t *shv, sh_video_frame_t *frame);
int sh_video_get_is_recording(sh_video_t *shv);
int sh_video_transcode(sh_video_t *shv, char *in, char *out);
void sh_video_release_frame(sh_video_frame_t *frame);
void sh_video_free(sh_video_t *shv);
void sh_video_recording_free(sh_video_recording_t *recording);

#endif
