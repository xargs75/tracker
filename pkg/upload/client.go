package upload

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"path"
)

type ClientConfig struct {
	Host      string
	TrackerId string
}

type Client struct {
	Config                  *ClientConfig
	httpClient              *http.Client
	Host                    string
	uploadVideoEndpoint     string
	uploadThumbnailEndpoint string
}

func NewClient(config *ClientConfig) *Client {
	client := Client{
		Config: config,
	}

	client.httpClient = &http.Client{}
	client.uploadVideoEndpoint = fmt.Sprintf("%s/UploadVideo", client.Config.Host)
	client.uploadThumbnailEndpoint = fmt.Sprintf("%s/UploadThumbnail", client.Config.Host)

	return &client
}

func (c *Client) Upload(eventId string, videoUri string, thumbnailUri string) error {
	var (
		file *os.File
		err  error
		req  *http.Request
	)

	file, err = os.Open(videoUri)

	if err != nil {
		panic(err)
	}
	defer file.Close()

	req, err = http.NewRequest("POST", c.uploadVideoEndpoint, file)

	if err != nil {
		log.Printf("%s\n", err)
		return err
	}

	if !setHeaders(&req.Header, c.Config.TrackerId, eventId, path.Base(videoUri)) {
		fmt.Printf("%+v\n", req.Header)
		return errors.New("failed to set headers")
	}

	resp, err := c.httpClient.Do(req)

	if err != nil {
		log.Printf("%s\n", err)
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return err
	}

	file, err = os.Open(thumbnailUri)

	if err != nil {
		log.Printf("%s\n", err)
		return err
	}
	defer file.Close()

	req, err = http.NewRequest("POST", c.uploadThumbnailEndpoint, file)

	if err != nil {
		log.Printf("%s\n", err)
		return err
	}

	if !setHeaders(&req.Header, c.Config.TrackerId, eventId, path.Base(thumbnailUri)) {
		return errors.New("failed to verify UploadThumbnail request")
	}

	resp, err = c.httpClient.Do(req)
	if resp.StatusCode != 200 {
		return err
	}
	defer resp.Body.Close()

	if err != nil {
		log.Printf("%s\n", err)
		return err
	}

	return err
}
