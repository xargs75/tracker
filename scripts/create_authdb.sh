#!/bin/bash
#htpasswd -bnBC 10 "" <password>

sqlite3 auth.db <<EOF
DROP TABLE IF EXISTS users;

CREATE TABLE users (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    deleted_at DATETIME,
    username TEXT,
    password TEXT,
    roles TEXT
);
CREATE INDEX idx_users_deleted_at ON users(deleted_at) ;
CREATE UNIQUE INDEX uix_users_username ON users("username") ;
EOF

sqlite3 auth.db <<EOF
INSERT INTO users (username, password, roles)
VALUES(
    'n4hy',
	'\$2y\$10\$QFd0DHXTawwNOHRVfktw6uAvVgcgcNl7AyKuIq.Q0J9j0/hEHb3XK',
    'admin'
);
INSERT INTO users (username, password, roles)
VALUES(
    'nibbleshift',
	'\$2y\$10\$FW1.XgeUyWsKMcvwLRiFv.0KIP9E8yHdIJ0a/z3Hj1DehX2tUx0fS',
    'admin'
);
INSERT INTO users (username, password, roles)
VALUES(
    'Griffindodd',
	'\$2a\$10\$oO35GFcF66qkUA.muPCJCeOIr/nWjAEbDZG9nzQ9MwcqRuEneXXcS',
    'admin'
);
EOF

